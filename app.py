# Tipe data dictionary / dalam JS disebut dengan tipe data Object
coffeShop = {
    "nama": "Toxins Coffe",
    "alamat": "Loji, Karawang",
    "makananFavorite": [
        "Nasi Bakar", "Nasi Tutug Oncom", "Nasi Liwet"
    ],
    "minumanFavorite": [
        "Kopi Sanggabuana", "Kopi Arabica", "Kopi Mochachinno", "Kopi Cappuchino"
    ]
}

print("Nama Kafe %s" %coffeShop["nama"])
print("Alamatnya di %s" %coffeShop["alamat"])


print("========Makanan Favorite=========")
for makanan in coffeShop.get("makananFavorite"):
    print(makanan)
print("========Minuman Favorite=========")
for minuman in coffeShop["minumanFavorite"]:
    print(minuman)



'''

String To Datetime

'''

from datetime import datetime as dt

date_str = '29-12-2021'

dto = dt.strptime(date_str, '%d-%m-%Y').date()
print(dto)
