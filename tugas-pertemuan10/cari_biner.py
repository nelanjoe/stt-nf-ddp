def cari_biner(data, n):
    awal = 0
    akhir = len(data) - 1
    tengah = 0

    while awal <= akhir:
        tengah = (akhir + awal) // 2

        if data[tengah] < n:
            awal = tengah + 1
        elif data[tengah] > n:
            akhir = tengah - 1
        else:
            return tengah

    return -1

target = 673
my_list = []
for i in range(1001):
    my_list.append(i)

hasil = cari_biner(my_list, target)
print(hasil)