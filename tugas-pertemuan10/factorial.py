'''
Factorial dengan Recrusive

'''

def factorial(n):
    if n <= 0 or n == 1:
        return n 
    else:
        return n * factorial(n - 1)


print(factorial(4))


'''

factorial dengan menggunakan iterasi/perulangan

'''

def factorialWhile(n):
    hasil = 1
    while n > 0:
        hasil = hasil * n
        n = n - 1
    return hasil


result = factorialWhile(4)
print(result)