'''

Fibbonaci recrusive

'''

def fib(n):
    if n in {0, 1}:
        return n
    return fib(n - 1) + fib(n - 2)

my_list = []

for i in range(15):
    my_list.append(fib(i))

print(my_list)


'''

Fibbonaci dengan iterasi/perulangan

'''


def fibonacci(n):
    a = 0
    b = 1
    if n < 0:
        return n
    elif n == 0:
        return 0
    elif n == 1:
        return b
    else:
        for i in range(1, n):
            c = a + b
            a = b
            b = c
        return b


my_list = []

for i in range(15):
    my_list.append(fibonacci(i))

print(my_list)
