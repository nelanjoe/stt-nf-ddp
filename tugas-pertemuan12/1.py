from datetime import datetime as dt

class Person:
    lingkungan = "RT 2/30 Kelurahan Majujaya"
    def __init__(self, name, tanggalLahir):
        self._namaLengkap = name
        self._tanggalLahir = tanggalLahir
        
    def tulis_deskripsi(self):
        date = dt.strptime(self._tanggalLahir, "%d-%m-%Y").date()
        return f"{self._namaLengkap} lahir pada {date}"

def main():
    orang1 = Person("Ahmad", "20-03-1987")
    orang2 = Person("Bayu", "12-05-1988")

    daftarWarga = [orang1, orang2]
    for o in daftarWarga:
        print(o.tulis_deskripsi())

main()