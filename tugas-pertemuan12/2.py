class Pegawai:
    honorPerJam = "30 Rb"
    def __init__(self, nama, jamKerja):
        self._nama = nama
        self._jamKerja = jamKerja

    def honor(self):
        return f"{self._nama} dengan honor {(self._jamKerja * 30000)}"


def main():
    budi = Pegawai("Budi", 8)
    print(budi.honor())

main()