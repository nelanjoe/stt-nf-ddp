class Person:
    def __init__(self, nama , tanggalLahir):
        self._nama = nama
        self._tanggalLahir = tanggalLahir
    
    def sayHello(self):
        return f"Hello nama saya {self._nama}, saya lahir {self._tanggalLahir}"
    
class Student(Person):
    def __init__(self, nama, tanggalLahir, tanggalMasuk):
        super().__init__(nama, tanggalLahir)
        self._tanggalMasuk = tanggalMasuk
    
    def durasiKuliah(self):
        return f"Saya masuk kulih pada tanggal {self._tanggalMasuk}"

def main():
    budi = Student("Budi", "20-2-2002", "20-7-2022");
    print(budi.sayHello())
    print(budi.durasiKuliah())

main()
        